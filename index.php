<?php
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);
require 'vendor/autoload.php';
require 'config.php';
use Inf\Router;

$dbFile = __DIR__.'/db/sqlite.db';
if(!file_exists($dbFile)){
    touch($dbFile);
}
define('DB_FILE', $dbFile); //display db errors?

$router = new Router([
  'paths' => [
      'controllers' => 'app/Controllers',
  ],
  'namespaces' => [
      'controllers' => 'App\Controllers',
  ],
]);



$router->get('/get_books', 'Book@getBooks');

// eg. http://localhost/interview/get_book/1
$router->get('/get_book/:id', 'Book@getBookById');

// eg. http://localhost/interview/save_book_comment
$router->post('/save_book_comment', 'Comment@saveBookComment');

// eg. http://localhost/interview/get_book_comments/1
$router->get('/get_book_comments/:id', 'Comment@getBookComments');


//$router->get('/get_sorted_characters/:string/:string?', 'Character@getSortedCharacters');
$router->get('/get_sorted_characters_by_name', 'Character@getSortedCharactersByNameOrderAsc');
$router->get('/get_sorted_characters_by_name_desc', 'Character@getSortedCharactersByNameOrderDesc');
$router->get('/get_sorted_characters_by_gender', 'Character@getSortedCharactersByGenderOrderAsc');
$router->get('/get_sorted_characters_by_gender_desc', 'Character@getSortedCharactersByGenderOrderDesc');

//$router->get('/get_filtered_characters/:string', 'Character@getFilteredCharacters');
$router->get('/get_characters_filtered_with_male', 'Character@getFilteredWithMaleCharacters');
$router->get('/get_characters_filtered_with_female', 'Character@getFilteredWithFemaleCharacters');


// This is just a test for adding a book comment
/* $router->get('/add_book_comment', function(){
  global $apiUrl;
  echo "<form action='$apiUrl/save_book_comment' method='post' >
            <label for='book_id'>Book ID:</label><br>
            <input type='text' id='book_id' name='book_id' value='1'><br>
            <label for='commenter'>Commenter:</label><br>
            <input type='text' id='commenter' name='commenter' value='John Snown'><br>
            <label for='comment'>Comment:</label><br>
            <textarea id='comment' name='comment'></textarea><br>
            <input type='submit' value='Submit'>
        </form>";
}); */


  $router->error(function() {
    // if it is ajax request
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest'){
        return json_encode(['error'=>'The page you are requesting can not be found']);
    }
  });

$router->run();