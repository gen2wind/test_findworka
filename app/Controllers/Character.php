<?php

namespace App\Controllers;

use Inf\Router\RouterRequest;

class Character extends Controller{

	function __construct(){
		parent::__construct();
	}


	public function getSortedCharactersByNameOrderAsc(){
		$page = RouterRequest::getData('page');
		$perPage = RouterRequest::getData('perPage');
		$data =  $this->helper->getCharacters($page,$perPage);
		// work on the charachers
		$characters = $data['characters'];

		// Perform sort
		usort($characters, function ($x, $y) {
			return strcasecmp($x['name'], $y['name']);
		});

		$data['characters'] = $characters;

		if($data){
			return $this->process($data)->send();
		}else{
			return $this->process($data,204,'text')->send();
		}
	}

	
	public function getSortedCharactersByNameOrderDesc(){
		$page = RouterRequest::getData('page');
		$perPage = RouterRequest::getData('perPage');
		$data =  $this->helper->getCharacters($page,$perPage);
		// work on the charachers
		$characters = $data['characters'];

		// Perform sort
		usort($characters, function ($x, $y) {
			return strcasecmp($x['name'], $y['name']);
		});
		

		// Perform Ordering if Descending
		$characters = array_values($characters);
		krsort($characters);
		$characters = array_values($characters);

		$data['characters'] = $characters;

		if($data){
			return $this->process($data)->send();
		}else{
			return $this->process($data,204,'text')->send();
		}
		
	}

	
	public function getSortedCharactersByGenderOrderAsc(){
		$page = RouterRequest::getData('page');
		$perPage = RouterRequest::getData('perPage');
		$data =  $this->helper->getCharacters($page,$perPage);
		// work on the charachers
		$characters = $data['characters'];

		// Perform sort
		usort($characters, function ($x, $y) {
			return strcasecmp($x['gender'], $y['gender']);
		});

		$data['characters'] = $characters;

		if($data){
			return $this->process($data)->send();
		}else{
			return $this->process($data,204,'text')->send();
		}
	}

	
	public function getSortedCharactersByGenderOrderDesc(){
		$page = RouterRequest::getData('page');
		$perPage = RouterRequest::getData('perPage');
		$data =  $this->helper->getCharacters($page,$perPage);
		// work on the charachers
		$characters = $data['characters'];

		// Perform sort
		usort($characters, function ($x, $y) {
			return strcasecmp($x['gender'], $y['gender']);
		});
		

		// Perform Ordering if Descending
		$characters = array_values($characters);
		krsort($characters);
		$characters = array_values($characters);

		$data['characters'] = $characters;

		if($data){
			return $this->process($data)->send();
		}else{
			return $this->process($data,204,'text')->send();
		}
		
	}
	
	/* public function getSortedCharacters($sort,$order = 'asc'){		
		$page = RouterRequest::getData('page');
		$perPage = RouterRequest::getData('perPage');

		$sortArr = ['name','age','gender'];	
		if(!in_array(strtolower($sort),$sortArr)){
			return $this->process(['error'=>'Please use keyword "name" or "age" or "gender" to sort characters'],203)->send();
		}	
		$data =  $this->helper->getCharacters($page,$perPage);

		// work on the charachers
		$characters = $data['characters'];

		// Perform sort
		if(in_array(strtolower($sort),$sortArr)){
			usort($characters, function ($x, $y) use($sort) {
				return strcasecmp($x[strtolower($sort)], $y[strtolower($sort)]);
			});
		}

		// Perform Ordering if Descending
		$characters = array_values($characters);
		if(\strtolower($order)==='desc'){
			krsort($characters);
			$characters = array_values($characters);
		}

		$data['characters'] = $characters;

		if($data){
			return $this->process($data)->send();
		}else{
			return $this->process($data,204,'text')->send();
		}
	} */



	public function getFilteredWithMaleCharacters(){		
		$page = RouterRequest::getData('page');
		$perPage = RouterRequest::getData('perPage');

		$data =  $this->helper->getCharacters($page,$perPage);

		// work on the charachers
		$characters = $data['characters'];
		// Perform filter
		$characters = $this->helper->arrayFilterByValue($characters,'gender','Male');
		$data['character_count'] = count($characters);
		$data['characters'] = $characters;

		if($data){
			return $this->process($data)->send();
		}else{
			return $this->process($data,204,'text')->send();
		}
	}

	

	public function getFilteredWithFemaleCharacters(){		
		$page = RouterRequest::getData('page');
		$perPage = RouterRequest::getData('perPage');

		$data =  $this->helper->getCharacters($page,$perPage);

		// work on the charachers
		$characters = $data['characters'];
		// Perform filter
		$characters = $this->helper->arrayFilterByValue($characters,'gender','Female');
		$data['character_count'] = count($characters);
		$data['characters'] = $characters;

		if($data){
			return $this->process($data)->send();
		}else{
			return $this->process($data,204,'text')->send();
		}
	}

	/* public function getFilteredCharacters($filter){		
		$page = RouterRequest::getData('page');
		$perPage = RouterRequest::getData('perPage');

		$filterArr = ['Male','Female'];	
		if(!in_array(ucfirst($filter),$filterArr)){
			return $this->process(['error'=>'Please use keyword "male" or "female" to filter characters by gender'],203)->send();
		}	
		$data =  $this->helper->getCharacters($page,$perPage);

		// work on the charachers
		$characters = $data['characters'];
		// Perform filter
		if(in_array(ucfirst($filter),$filterArr)){
			$characters = $this->helper->arrayFilterByValue($characters,'gender',ucfirst($filter));
		}
		$data['character_count'] = count($characters);
		$data['characters'] = $characters;

		if($data){
			return $this->process($data)->send();
		}else{
			return $this->process($data,204,'text')->send();
		}
	} */
}