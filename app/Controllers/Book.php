<?php

namespace App\Controllers;

use Inf\Router\RouterRequest;

class Book extends Controller{

	function __construct(){
		parent::__construct();
	}
	
	
	public function getBooks(){
		$page = RouterRequest::getData('page');
		$perPage = RouterRequest::getData('perPage');

		$data =  $this->helper->getBooks($page,$perPage);
		if($data){
			return $this->process($data)->send();
		}else{
			return $this->process($data,204,'text')->send();
		}
	}

	
	public function getBookById($bookId){		
		$data =  $this->helper->getBook($bookId);
		if($data){
			return $this->process($data)->send();
		}else{
			return $this->process($data,204,'text')->send();
		}
	}
}