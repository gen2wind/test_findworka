<?php

namespace App\Controllers;

use Inf\Router\RouterRequest;

class Comment extends Controller{

	function __construct(){
		parent::__construct();
	}


	public function saveBookComment(){
		$bookId = RouterRequest::postData('book_id',true);
		$commenter = RouterRequest::postData('commenter', true);
		$comment = RouterRequest::postData('comment', true);
		$error = [];
		if(empty($bookId)){
			$error[] = "Please Select the book You are writting comment on";
		}
		if(empty($comment)){
			$error[] = "Comment cannot be emptied";
		}
		if(strlen($comment) > 500){
			$error[] = "Comment should not be more than 500 characters";
		}

		if(!empty($error)){
			return $this->process(['errors'=>$error],203)->send();
		}else{
			date_default_timezone_set("UTC");
			$data = [
				'book_id'=>$bookId,
				'commenter'=>$commenter,
				'comment'=>$comment,
				'ip_address'=>RouterRequest::ip_address(),
				'date'=>date("Y-d-mTG:i:sz", time()),
			];
			date_default_timezone_get();
			$submit = $this->db->insert('comments', $data );
			if($submit){			
				return $this->process(['status' => 1, 'txt' => 'A comment has been added to book','newId'=>$this->db->lastid()])->send();
			}else{
				return $this->process(['status' => 0, 'txt' => 'Unable to add comment at the moment'],203)->send();
			}
		}
	}

	public function getBookComments($bookId){
		$sql = "SELECT * FROM `comments` WHERE `book_id` =  $bookId ORDER BY `date` DESC ";
		$data = $this->db->fetchAllRows($sql);

		if($data){			
			return $this->process(['status' => 1, 'txt' => 'Comments are successfully fetched', 'comments' => $data ])->send();
		}else{
			return $this->process(['status' => 0, 'txt' => 'No comments for the book at the moment'],203)->send();
		}
	}
}